$(function () {
    'use strict';

    function lg(obj) {
        console.log(obj);
    };

    $(document).ready(function () {
       $.ajax({url: '/getamount', success: function(result){ 
           $('.blog-amount').html(result);        
       }});
   
       $.ajax({url: '/getamountsummary', success: function(result){ 
           $('.blog-amount-active').html(result.active);     
           $('.blog-amount-draft').html(result.draft); 
           $('.blog-amount-archive').html(result.archive);   
       }}); 

       $(document).on('click', '.item .delete', function () {
            var obj = $(this);
            
            lg(obj);

            $.ajax({url: obj.attr('href'), success: function(result){
                if (result === '1') {
                    obj.parent().remove();
                    $('.ajax-message').html('Removed succesfully by AJAX');
                } else {
                    $('.ajax-message').html('Removing failed by AJAX (internal error)');
                }                
                lg(result);
            }, error: function(xhr,status,error) {
                $('.ajax-message').html(status + ' ' + error + ' Cannot connect to server (internal error)');
            }});
        
            return false;
        });
    });    
});