<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('hello', 'SiteController@hello');

Route::get('grid', 'SiteController@grid'); //для вывода данных с БД в браузер

Route::get('grid/{q}', 'SiteController@grid'); //для поиска

Route::get('view/{id}', 'SiteController@view');

Route::get('create', 'SiteController@create');

Route::get('update/{id}', 'SiteController@update');

Route::get('delete', 'SiteController@delete');

Route::post('store', 'SiteController@store');

//API methods
Route::get('getamount', 'SiteController@getamount'); //для вывода кол-ва записей из БД

Route::get('getamountactive', 'SiteController@getamountactive');

Route::get('getamountdraft', 'SiteController@getamountdraft');

Route::get('getamountarchive', 'SiteController@getamountarchive');

Route::get('getamountsummary', 'SiteController@getamountsummary');