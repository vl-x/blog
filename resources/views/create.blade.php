@extends('layouts.app')

@section('title', 'Create blog')

@section('content')
    <h1>create</h1>
       @include('_form', ['blog' => $blog])
    </body>
@endsection
