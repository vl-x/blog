@extends('layouts.app')

@section('title', 'Grid')

@section('content')
    <h1>Grid</h1>
    @if (session('status'))
         <div class="alert alert-success">
             {{ session('status') }}
         </div>
    @endif
    <div class="ajax-message">
        
    </div>
    <hr>
    <form action="" method="get">
        <input type="text" placeholder="search title" name="q"><br>
        <input type="submit" value="search">
    </form>
    <hr>
    <h4>Общее количество записей: <span class="blog-amount">тут будет результат js</span></h4>
    <h5>из них:</h5>
    <h5>опубликовано - <span class="blog-amount-active">тут будет результат js</span></h5>
    <h5>черновики - <span class="blog-amount-draft">тут будет результат js</span></h5>
    <h5>архив - <span class="blog-amount-archive">тут будет результат js</span></h5>
    <?php foreach ($grid as $item) : ?>
        <div class="item">
            <h1><?php echo $item->title; ?></h1>
            <p><?php echo $item->body; ?></p>
            <a href="/view/<?php echo $item->id; ?>">View</a><br>
            <a href="/update/<?php echo $item->id; ?>">Update</a><br>
            <a data-id="<?php echo $item->id; ?>" class="delete" href="/delete?id=<?php echo $item->id; ?>">Delete</a><br>
            <hr>
        </div>
    <?php endforeach?>
    
    <?php if ($perpage=='all') : ?>
        <a href="/grid?q=<?php echo $q?>">Вернуть пагинацию</a>
    <?php else : ?>
        <?php echo $grid->links()?>
        <a href="/grid?perpage=all&q=<?php echo $q?>">Показать всё</a>
    <?php endif ?>
@endsection