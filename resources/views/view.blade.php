@extends('layouts.app')

@section('title', 'View')

@section('content')
    <h1><?php echo $blog->title;?></h1>
    <p><?php echo $blog->body;?></p>
    <small><?php echo $blog->updated_at;?></small>
@endsection