@extends('layouts.app')

@section('title', 'Update blog')

@section('content')
    <h1>update</h1>

    <h2><?php echo $blog->title;?></h2>
    <p><?php echo $blog->body;?></p>
    <small><?php echo $blog->updated_at;?></small>

    @include('_form', ['blog' => $blog])
@endsection
