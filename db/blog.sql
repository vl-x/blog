-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 07 2018 г., 13:58
-- Версия сервера: 5.6.38
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `body` longtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog`
--

INSERT INTO `blog` (`id`, `title`, `body`, `created_at`, `updated_at`, `status`) VALUES
(35, 'title 01 lorem', 'blablablabla', '2018-03-20 13:41:02', '2018-03-20 13:41:02', 1),
(36, 'title 02', 'blablablabla', '2018-03-20 13:41:11', '2018-03-20 13:41:11', 5),
(37, 'title 03', 'lorem', '2018-03-20 13:41:24', '2018-03-20 13:41:24', 1),
(38, 'title 04', 'blablablabla', '2018-03-20 13:41:35', '2018-03-20 13:41:35', 5),
(39, 'title 05', 'blablablabla', '2018-03-20 13:41:50', '2018-03-20 13:41:50', 1),
(40, 'fixed title', 'lorem', '2018-03-21 14:27:59', '2018-03-21 14:27:59', 10),
(41, 'new item 06', 'попопоп', '2018-03-22 14:16:40', '2018-03-22 14:16:40', NULL),
(42, 'new item 07', 'vvv', '2018-03-22 14:24:16', '2018-03-22 14:24:44', 5);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
