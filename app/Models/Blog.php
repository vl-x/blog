<?php

namespace App\Models; //задаём родительскую папку файла Blog.php

use Illuminate\Database\Eloquent\Model; //задаём где лежит класс Model (для того чтобы с него наследоваться в класс Blog)

class Blog extends Model
{
    protected $table = 'blog';
    protected $fillable = ['id', 'title', 'body', 'status'];
    static function search($data) {
        $q = isset($data['q'])?$data['q']:null;
        $perpage = isset($data['perpage'])?$data['perpage']:null; //$perpage = желаемое количество постов, которое хочет увидеть пользователь на странице 
        $numPage = isset($data['numPage'])?$data['numPage']:2; //$numPage = количество отображаемых на странице постов по умолчанию (2 на странице)
                
        
        if ($q) {
            $grid = Blog::active()
                    ->where('title', 'like', '%' . $q . '%')
                    ->orWhere('body', 'like', '%' . $q . '%');
            if ($perpage=='all') {
                $grid = $grid->get();
            } else {
                $grid = $grid->paginate($numPage);
            }
        } else {
            if ($perpage=='all') {
                $grid = Blog::active()->get();
            } else {
                $grid = Blog::active()->paginate($numPage);
            }
        }
        
        return $grid;
    }
    
    public function scopeActive($query) {
        return $query->whereStatus('1');
    }
    
    public function scopeDraft($query) {
        return $query->whereStatus('5');
    }
    
    public function scopeArchive($query) {
        return $query->whereStatus('10');
    }   
}

