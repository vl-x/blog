<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function hello()
    {
        return view('hello');
    }

    public function create() //метод create - только для отображения вьюшки формы создания новой записи
    {
        $blog = new Blog();
        
        $blog->title='fixed title';
        return view('create', ['blog' => $blog]);
    }

    public function grid(Request $request)
    {
        $data = $request->all();
        $perpage = isset($data['perpage'])?$data['perpage']:null;
        $q = isset($data['q'])?trim($data['q']):null; //текст, введённый пользователем в инпуте search
        $grid = Blog::search($data);

        
        return view('grid', ['grid' => $grid, 'perpage' => $perpage, 'q' => $q]);
    }
    
    public function getamount() {
        $amount = count(Blog::all()); 
        return $amount;
    }

    public function getamountsummary() {
        $amountSummary['active'] = count(Blog::active()->get()); 
        $amountSummary['draft'] = count(Blog::draft()->get()); 
        $amountSummary['archive'] = count(Blog::archive()->get()); 
        return $amountSummary;
    }
    
    public function view($id) /*view=read*/ {
        return view('view', ['blog' => Blog::findOrFail($id)]);
    }

    public function update($id) //метод update - только для отображения вьюшки формы обновления старой записи с предзаполненными инпутами
    {
        $item = Blog::findOrFail($id); //Blog - название таблицы в БД
        return view('update', ['blog' => $item]);
    }

    public function delete(Request $request) {
        $data = $request->all();
        $id = isset($data['id'])?$data['id']:null;
        $item = Blog::find($id);
        $res = $item->delete(); //%res = результат удаления
        if ($request->ajax()) {
            return (string)$res;
        }

        return redirect('/grid')->with('status', 'Item deleted!'); /* содержимое 
          уведомления записывается в переменную $status и сохраняется в сессии
          для дальнейшего вывода во вьюшке grid */
    }

    public function store(Request $request) //$request - глобальная переменная $_REQUEST в виде объекта
    {
        $data = $request->all(); // "->all()" преобразует объект в массив 
        $id=(int)$data['id'];
        $item = Blog::find($id); // объект
        if ($item) {
            $item->fill($data);
            $item->save();
        } else {
            Blog::create($data);
        }
        return redirect('/grid')->with('status', 'Item saved!');
    }    
}